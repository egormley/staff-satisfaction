<?php
  require_once('includes/setup.php');

  if((int)$_SESSION['user_id'] > 0) {
    echo 'Congratulations you are logged in';
  }
  else {
    header('location:logout.php', 403);
  }
?>
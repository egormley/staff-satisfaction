<?php

class Database {
  private $_db;
  public function __construct() {
    $this->_db = new PDO('mysql:host=localhost;dbname=staff', 'root', '');
    $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function saveUser($username, $pwd) {
    $stmt = $this->_db->prepare('INSERT INTO users(username, password) VALUES(?, ?)');
    $stmt->execute([$username, $pwd]);
    return $this->_db->lastInsertId();
  }

  public function saveOrg($orgname, $desc) {
    $stmt = $this->_db->prepare('INSERT INTO organisations(org_name, description) VALUES(?, ?)');
    $stmt->execute([$orgname, $desc]);
    return $this->_db->lastInsertId();
  }

  public function updateStatus($orgId, $orgStatus, $userId) {
    $stmt = $this->_db->prepare('UPDATE users SET org_id = ?, org_status = ? WHERE user_id = ?');
    return $stmt->execute([$orgId, $orgStatus, $userId]);
  }

  public function getUser($username) {
    $stmt = $this->_db->prepare('SELECT user_id, password FROM users WHERE username = ?');
    $stmt->execute([$username]);
    return $stmt->fetch(PDO::FETCH_OBJ);
  }
}

?>
<?php
  abstract class Tools {
    public static function randString($length, $nums = false) {
      if ($nums) {
        $chars = "0123456789";
      } else {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      }

      $size = strlen($chars);
      $str = (string)null;
      for($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
      }
      return $str;
    }

    // Create a valid Blowfish salt
    public static function blowfishSalt() {
      return '$2a$10$'.Tools::randString(21).'$';
    }
    public static function comparePass($pass, $hash) {
      $salt = substr($hash, 0, strpos($hash, '.'));
      $login = crypt($pass, $salt.'$');

      return $login === $hash;
    }
  }

?>
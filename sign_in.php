<?php
  require_once('includes/setup.php');

  if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['pwd'];
    $userdetails = $db->getUser($username);

    if ($userdetails === false) {
      echo 'User does not exist';
    }
    elseif (Tools::comparePass($password, $userdetails->password)) {
      $_SESSION['username'] = $username;
      $_SESSION['user_id'] = $userdetails->user_id;

      header('location:loggedin.php', 200);
    } else {
      echo 'Incorrect password';
    }
  }
    include('templates/global.header.php');
?>

  <form method="post">
    <fieldset>
      <legend>Sign In</legend>
      <input type="text" placeholder="Username" name="username">
      <input type="text" placeholder="Password" name="pwd">
      <input type="submit" name="submit" value="Sign in!">
    </fieldset>
  </form>

  <?php
    include('templates/global.footer.php');
  ?>
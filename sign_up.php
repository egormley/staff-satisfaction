<?php
  require_once('includes/setup.php');

  if (isset($_POST['submit'])) {

    $username = $_POST['username'];
    $password = $_POST['pwd'];

    $salt = Tools::blowfishSalt();
    $hash = crypt($password, $salt);
    $userid = $db->saveUser($username, $hash);
  }

  include('templates/global.header.php');
?>

  <form method="post">
    <fieldset>
      <legend>Sign Up</legend>
      <input type="text" placeholder="Username" name="username">
      <input type="text" placeholder="Password" name="pwd">
      <input type="submit" name="submit" value="Sign up!">
    </fieldset>
  </form>
  <?php
    include('templates/global.footer.php');
  ?>


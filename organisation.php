<?php
  require_once 'includes/database.php';

  $orgName = 'BBC';
  $orgDesc = 'Media organisation';
  $orgStatus = 0;
  $userid = 2;//$_SESSION["user_id"];

  $db = new Database();
  $orgId = $db->saveOrg($orgName, $orgDesc);
  echo $db->updateStatus($orgId, $orgStatus, $userid);
?>